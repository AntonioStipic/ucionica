var express = require("express");
var bodyParser = require("body-parser");
var mysql = require("mysql");
var cookieParser = require("cookie-parser");
var crypto = require("crypto");
var session = require("express-session");

var app = express();


app.use(express.static(__dirname + "/static"));
app.set('views', __dirname + '/static/views');

app.use(session ({
	secret: "secret",
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.engine('html', require('ejs').renderFile);

var connection = mysql.createConnection ({
	host: "localhost",
	user: "root",
	password: "",
	database: "ucionica"
});

setInterval(function () {
    connection.query("SELECT 1");
}, 5000);

app.listen(process.env.PORT || 3000, function () {
	console.log("Listening on port 3000");
});

app.get("/home", function (request, response) {
	response.sendFile(__dirname + "/static/views/home.html");
});

app.get("/history", function (request, response) {
	response.sendFile(__dirname + "/static/views/history.html")
})

app.post("/getHistory", function (request, response) {
	var query = connection.query("SELECT * FROM log", function(error, result) {
		if (error) {
			console.log("DB getHistory error", error);
		}
		response.send(result);
	});
});

app.post("/new", function (request, response) {
	var body = request.body;
	var datum = body.datum;
	var smjena = body.smjena;
	var sat = body.sat;
	var napomena = body.napomena;
	var racunala = body.pcs;

	var returns = "success";

	var query = connection.query("INSERT INTO log (id, datum, smjena, sat, racunala, napomena) VALUES (null, '" + datum + "', " + smjena + ", " + sat + ", '" + racunala + "', '" + napomena + "');", function(error, result) {
		if (error) {
			console.log("DB INSERT error", error);
			returns = "fail";
		}
	});
	response.send(returns);
});